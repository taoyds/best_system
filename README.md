
## Dwnloading the Software
* **Using Git** : ```$ git clone git@bitbucket.org:taoyds/best_system.git```. This requires that you have a bitbucket account and have uploaded your public ssh key.

## Compatibility and Dependencies
Python 2.7 and 3.x
sklearn
scipy
numpy
pandas
nltk

## Running the System
```
usage: python best_run.py --test [path to test directory where includes ere and source (in exactly the same name) folders] --we [path to word2vec] --ngram [e.g. 123]

word2vec is a big file, you should download here: https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit
```

## Running Example

```
usage: python best_run.py --test ../data/test/ --ngram 12 --we GoogleNews-vectors-negative300.bin

outprints:

loading train model pkg...
loading testing data and extracting features from source, ere, and files...
There are total 1 files to be processed (MAKE SURE each file ends with .rich_ere.xml in ere directory)
processing file is: ENG_DF_000183_20150409_F0000009F_0-7277 

in file ENG_DF_000183_20150409_F0000009F_0-7277 ---------------
there are 200 sentences.

There are TOTAL 200 sentences in the input files.

cleaning data and add lex indicators...
loading word embedding...
building test features --- ngram part...
building test features --- pos embedding part...
In embed process --- the embed matrix shape: (142, 3600)
combining log-count ratio and pos embedding features...
running model...
testing...

-------------pred y_dev counts------------
[[  0  25]
 [  1 117]]
Testing Done! Successfully write the predicted best xml files to ../data/test/.

```

# More comming soon #
