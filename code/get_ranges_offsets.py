#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This module contains help functions to read source files, add author says, and
get post/quote ranges and author offsets in both original source files and new
converted source files.
'''
import sys
import re
import os
from nltk.tree import Tree
from os import listdir, makedirs
from nltk import ne_chunk, pos_tag, word_tokenize, tokenize
from os.path import isfile, isdir, join, splitext, split, exists


def get_chunks(text):
    '''
    return a list of NERs in text
    '''
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    current_chunk = []
    for i in chunked:
        if type(i) == Tree:
            current_chunk.append([token for token, pos in i.leaves()])
    return current_chunk


def transform_source(src_path):
    '''
    Add 'The author says ' to the beginning of each detected sentence in each source file
    Store the indexs of the beginning of each sentence in the original source file

    Because we are going to change the original offsets in ere and best files
    only based on the number of 'The author says ' added and the positions of them,
    it is crucial to not add or delete any space (even empty space) but only 'The author says '
    '''
    src_original = open(src_path, 'r')
    author_say = 'The author says '
    sent_num = 0
    src_content = ''
    with src_original as f:
        for line in f:
            line = unicode(line, "utf-8")
            if (not line.startswith('<') and line != '\n') or line.startswith('<a href') or line.startswith('<img'): #if the line is a real text
                line_add_author = ''
                sent_start_inds = []
                # print repr(line)
                sents = tokenize.sent_tokenize(line)

                for sent in sents: #sent_tokenize will trimed out empty spaces bw sentences
                    sent_num += 1
                    sent_start_ind = line.find(sent)
                    sent_start_inds.append(sent_start_ind)

                sent_start_inds[0] = 0
                for ind in xrange(len(sent_start_inds)):
                    sent_start_ind = sent_start_inds[ind]
                    if ind + 1 == len(sent_start_inds):
                        sent = line[sent_start_ind:]
                    else:
                        sent_end_ind = sent_start_inds[ind + 1]
                        sent = line[sent_start_ind: sent_end_ind]
                    ne_chunks = [word for ne_chunk in get_chunks(sent) for word in ne_chunk]
                    first_word = word_tokenize(sent)[0] if word_tokenize(sent) else None
                    # print 'first_word is: ', first_word
                    # print first_word in ne_chunks
                    if len(sent) > 0 and first_word not in ne_chunks and first_word != 'I':
                        sent = sent[0].lower() + sent[1:] #TODO: use ne_chunk
                    line_add_author += author_say + sent
                src_content += line_add_author
            else:
                src_content += line

    return src_content


class SrcRange(object):
    '''
    Represent a set of index ranges of author_offsets, quote_begins, quote_ranges and post_ranges
    '''
    def __init__(self):
        self.author_offsets = []
        self.quote_begins = []
        self.post_ranges = {}
        self.quote_ranges = {}
        self.author_inds = [] #the indexs of added 'The author says ' in the new source
        self.author_inds_original = [] #the indexs of added 'The author says ' in the original source
        self.offset_change_ranges = {} #a dict stores what number old offsets should add to get new offsets

    def build_ranges(self, content):
        #find the indexs of added 'The author says ' in the new source file
        author_say = 'The author says '
        author_inds = [m.start() for m in re.finditer(author_say, content)]
        #find the indexs of added 'The author says ' in the original source file based on author_inds above
        #and offset_change_ranges is a dict whose
        #key: the number should be added to the old offset in order to get the new offset;
        #value: the range of old offsets that should add the number
        add_num = 0
        author_inds_original = []
        offset_change_ranges = {}
        pre_ind = 0
        for i in author_inds:
            offset_movedback = add_num * len(author_say)
            ind = i - offset_movedback
            author_inds_original.append(ind)
            if len(author_inds_original) > 1:
                pre_ind = author_inds_original[add_num-1]
            offset_change_ranges[offset_movedback] = [pre_ind, ind]
            add_num += 1

        self.author_inds = author_inds
        self.author_inds_original = author_inds_original
        self.offset_change_ranges = offset_change_ranges


        author_offsets = [m.end() for m in re.finditer('post author="', content)]
        author_offsets_additions = [m.end() for m in re.finditer('id=".*" author="', content)]
        author_offsets.extend(author_offsets_additions)
        author_offsets_additions_v2 = [m.end() for m in re.finditer('datetime=".*" author="', content)]
        author_offsets.extend(author_offsets_additions_v2)
        if len(author_offsets_additions) == 0:
            author_offsets_additions_v2 = [m.end() for m in re.finditer('<post datetime="', content)]
            author_offsets.extend(author_offsets_additions_v2)
        author_offsets_news = [m.end() for m in re.finditer('<P>', content)]
        author_offsets.extend(author_offsets_news)
        author_offsets_v3 = [m.end() for m in re.finditer('<AUTHOR>', content)] #for new newswire
        author_offsets.extend(author_offsets_v3)
        author_offsets = sorted(author_offsets)
        self.author_offsets = author_offsets
        post_ends = [m.end() for m in re.finditer('</post>', content)]
        post_ends_news = [m.end() for m in re.finditer('</P>', content)]
        post_ends.extend(post_ends_news)
        if len(author_offsets_v3) >= 1:
            post_ends_v3 = [m.end() for m in re.finditer('</TEXT>', content)] #for new newswire
            post_ends.extend(post_ends_v3)
        post_ends = sorted(post_ends)
        if len(author_offsets) != len(post_ends):
            print 'Warning: len of author_offsets is not the same as post_ends!'
            print author_offsets, post_ends
            print '\n'

        quote_begins = [m.end() for m in re.finditer(' orig_author="', content)]
        # quote_begins_additions = [m.end() for m in re.finditer('Originally Posted by ', content)]
        # quote_begins.extend(quote_begins_additions)
        quote_begins_additions_2 = [m.end() for m in re.finditer('<quote orig_datetime=', content)]
        quote_begins.extend(quote_begins_additions_2)
        special_cases = [m.end() for m in re.finditer('<quote>', content)]
        # quote_begins.extend(special_cases)
        quote_begins = sorted(quote_begins)
        self.quote_begins = quote_begins
        quote_ends = [m.end() for m in re.finditer('</quote>', content)]
        #comment if '<quote>' is not the same as author
        for sp_ind in special_cases:
            for q_end in quote_ends:
                if q_end > sp_ind:
                    quote_ends.remove(q_end)
                    break
        if len(quote_begins) != len(quote_ends):
            print 'Warning: len of quote_begins is not the same as quote_ends!'
            print quote_begins, quote_ends
            print '\n'

        #build ranges of quotes
        quote_ranges = {q_begin : [] for q_begin in quote_begins}
        quote_outer_ranges =[]
        q_ind = 0
        q_begin_stack = []
        q_end = []
        if quote_begins:
            cur_q_begin = quote_begins[q_ind]
            most_inner_continue_flag = False
            for q_end in quote_ends:
                most_inner = False
                cur_end_ind = quote_ends.index(q_end)
                while cur_q_begin < q_end:
                    if q_ind >= len(quote_begins):
                        break
                    most_inner = True
                    q_begin_stack.append(cur_q_begin)
                    if len(q_begin_stack) >= 2:
                        last_q_begin = q_begin_stack[len(q_begin_stack)-2]
                        if most_inner_continue_flag:
                            last_q_end = quote_ends[cur_end_ind - 1]
                            quote_ranges[last_q_begin].append([last_q_end, cur_q_begin])
                            most_inner_continue_flag = False
                        else:
                            quote_ranges[last_q_begin].append([last_q_begin, cur_q_begin])
                    q_ind += 1
                    if q_ind < len(quote_begins):
                        cur_q_begin = quote_begins[q_ind]
                stack_top = q_begin_stack.pop()

                if cur_end_ind + 2 <= len(quote_ends) and q_begin_stack:
                    next_q_end = quote_ends[cur_end_ind + 1]
                    if q_end < cur_q_begin < next_q_end:
                        most_inner_continue_flag = True

                cur_top = 0
                if most_inner:
                    cur_top = stack_top
                else:
                    cur_top = quote_ends[cur_end_ind - 1]

                quote_ranges[stack_top].append([cur_top, q_end])
                if not q_begin_stack:
                    quote_outer_ranges.append([stack_top, q_end])
        self.quote_ranges = quote_ranges

        #build the ranges of posts based on quote_ranges
        post_ranges = {}
        for i in xrange(len(author_offsets)):
            author_offset = author_offsets[i]
            post_end = post_ends[i]
            post_range = []
            if quote_outer_ranges:
                quote_outer_ranges = sorted(quote_outer_ranges)
                range_top = None
                range_bom = None
                for j in xrange(len(quote_outer_ranges)):
                    if range_top:
                        if author_offset <= quote_outer_ranges[j][0] <= range_top[0]:
                            range_top = quote_outer_ranges[j]
                    else:
                        if author_offset <= quote_outer_ranges[j][0]:
                            range_top = quote_outer_ranges[j]
                    if range_bom:
                        if range_bom[1] <= quote_outer_ranges[j][1] <= post_end:
                            range_bom = quote_outer_ranges[j]
                    else:
                        if quote_outer_ranges[j][1] <= post_end:
                            range_bom = quote_outer_ranges[j]

                quotes_in_post = []
                if range_top and range_bom:
                    quotes_in_post = quote_outer_ranges[quote_outer_ranges.index(range_top):quote_outer_ranges.index(range_bom)+1]
                    quote_outer_ranges = [x for x in quote_outer_ranges if x not in quotes_in_post]

                if quotes_in_post:
                    post_range.append([author_offset, quotes_in_post[0][0]])
                    if len(quotes_in_post) == 1:
                        post_range.append([quotes_in_post[0][1], post_end])
                    else:
                        post_range.append([quotes_in_post[0][1], quotes_in_post[1][0]])
                        for q in xrange(1, len(quotes_in_post)):
                            if q + 1 < len(quotes_in_post):
                                post_range.append([quotes_in_post[q][1], quotes_in_post[q+1][0]])
                            else:
                                post_range.append([quotes_in_post[q][1], post_end])
                    post_ranges[author_offset] = post_range

            if author_offset not in post_ranges.keys() or not post_ranges[author_offset]:
                post_range.append([author_offset, post_end])
                post_ranges[author_offset] = post_range

        self.post_ranges = post_ranges


def get_src_ranges(source_path):
    '''
    get all ranges of authors, new added 'The author says', posts and quotes
    '''
    src_ranges = SrcRange()
    with open(str(source_path)) as f:
        content = ''
        for line in f:
            line = unicode(line, "utf-8")
            content += line
        src_ranges.build_ranges(content)
    return src_ranges

def get_sent_begins(source_path):
    '''
    get a list of the indexes of sentences in the source file
    '''
    src_ranges = SrcRange()
    content_w_authorsays = transform_source(source_path)
    src_ranges.build_ranges(content_w_authorsays)
    sent_begins = src_ranges.author_inds_original
    return sent_begins
