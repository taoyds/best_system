#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This module takes source, ere, and best files, extracts sentences, features, and
annotations to produce a json file with all info for training and testing.
'''
import sys
import re
import os
import json
import codecs
import pickle
import pandas as pd
import string
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import xml.etree.ElementTree as ET
from os import listdir, makedirs
from os.path import isfile, isdir, join, splitext, split, exists
from get_ranges_offsets import SrcRange, get_sent_begins
from nltk import ne_chunk, pos_tag, word_tokenize, tokenize
from nltk.parse.stanford import StanfordDependencyParser, StanfordParser

#set up StanfordParser dirs
path_to_jar = '/Users/taoyds/Documents/workspace/stanford-corenlp-full-2015-12-09/stanford-corenlp-3.6.0.jar'
path_to_models_jar = '/Users/taoyds/Documents/workspace/stanford-corenlp-full-2015-12-09/stanford-corenlp-3.6.0-models.jar'
parser = StanfordParser(path_to_jar=path_to_jar, path_to_models_jar=path_to_models_jar)
stanford_tokenizer = tokenize.StanfordTokenizer(path_to_jar=path_to_jar)

#sentiment lexcion dirs
all_dir = "/Users/taoyds/Documents/BeSt/codes/lexcion/list_all_set.p"
neg_dir = "/Users/taoyds/Documents/BeSt/codes/lexcion/neg_set.p"
pos_dir = "/Users/taoyds/Documents/BeSt/codes/lexcion/pos_set.p"


def usage():
    print ("python feature_extractor.py <input dir>")
    print ("<input dir> is the dir where contains source, ere, and annotation folders.")


def remove_tags(text):
    '''
    remove URLs and other special symbols
    '''
    text = re.sub('<[^>]*>', '', text)
    return re.sub('/', ' ', text)


def mid_offset_dicts_getter(root):
    '''
    get dicts of entity mention to its offset and relation mention to its arg1's offset
    used to spot offset of targets of relations in best files
    '''
    offset_emid = {int(em.get('offset')) : em.get('id') for em in root.iter('entity_mention')}
    emid_offset = {em.get('id') : int(em.get('offset')) for em in root.iter('entity_mention')}
    emid_endoffset = {em.get('id') : int(em.get('offset'))+ int(em.get('length')) for em in root.iter('entity_mention')} #int(em.get('offset') + em.get('length'))
    relm_offset = {relm.get('id') : emid_offset[relm.find('rel_arg1').get('entity_mention_id')] for relm in root.iter('relation_mention')}
    fid_offset = {f.get('id') : int(f.get('offset')) for f in root.iter('filler')} #ere_tree
    fid_endoffset = {f.get('id') : int(f.get('offset'))+int(f.get('length')) for f in root.iter('filler')} #ere_tree

    return (offset_emid, emid_offset, emid_endoffset, relm_offset, fid_offset, fid_endoffset)


def sent_pq_newline_index(sent_range, pq_ranges, newline_ranges):
    '''
    get post/quote and paragraph indexs where the sentence is located at
    '''
    sent_range1, sent_range2 = sent_range[0], sent_range[1]
    pq_ind = 0
    newline_ind = 0
    for pq_offset, pq_rangs in pq_ranges.items():
        for pq_range in pq_rangs:
            pq_range1, pq_range2 = pq_range[0], pq_range[1]
            if pq_range1 < sent_range1 and sent_range2 < pq_range2:
                pq_ind = pq_offset
                break
    for newline_offset, newline_range in newline_ranges.items():
        newline_range1, newline_range2 = newline_range[0], newline_range[1]
        if newline_range1 < sent_range1 and sent_range2 < newline_range2:
            newline_ind = newline_offset
            break
    return (pq_ind, newline_ind)


def get_sent_ranges(source_path):
    '''
    get all ranges of authors, new added 'The author says', posts and quotes
    '''
    if isfile(source_path):
        src_ranges = SrcRange()
        with open(str(source_path)) as f:
            content = ''
            for line in f:
                line = unicode(line, "utf-8")
                content += line

            src_ranges.build_ranges(content)
            # print 'content with whitespace is: ', repr(content)
            # author_say_inds = src_ranges.author_inds
            author_say_inds = get_sent_begins(source_path)
            # print 'author_say_inds is: ', author_say_inds
            src_ranges.author_inds = author_say_inds
            post_ranges = src_ranges.post_ranges
            quote_ranges = src_ranges.quote_ranges
            pq_ranges = post_ranges.copy()
            pq_ranges.update(quote_ranges)
            newline_inds = [m.start() for m in re.finditer('\n', content)]
            newline_ranges = {i : (newline_ind, newline_inds[i+1]) for i, newline_ind in enumerate(newline_inds) if i+1 < len(newline_inds)}
            sent_ranges = [(author_say_ind, author_say_inds[i+1]) for i, author_say_ind in enumerate(author_say_inds) if i+1 < len(author_say_inds)]
            last_range = (author_say_inds[len(author_say_inds)-1], len(content))
            sent_ranges.append(last_range)
            # print 'author_say_inds is: ', author_say_inds
            # print 'sent_ranges is: ', sent_ranges
            range_sent = {}
            for sent_range in sent_ranges:
                sent_beg = sent_range[0]
                sent_end = sent_range[1]
                sent_text = content[sent_beg:sent_end]
                if '\n' in sent_text:
                    # print 'sent_range is: ', sent_range
                    # print 'sent_text before split is: ', sent_text
                    splited_sent = sent_text.split('\n')
                    sent_text = splited_sent[0]
                    # print 'splited_sent is: ', splited_sent
                    # print 'sent_text after is: ', sent_text
                    for newline_ind in newline_inds:
                        if sent_beg < newline_ind:
                            sp_range = (sent_beg, newline_ind)
                            pq_ind, newline_ind = sent_pq_newline_index(sp_range, pq_ranges, newline_ranges)
                            range_sent[sp_range] = (sent_text, pq_ind, newline_ind)
                            break
                else:
                    pq_ind, newline_ind = sent_pq_newline_index(sent_range, pq_ranges, newline_ranges)
                    range_sent[sent_range] = (sent_text, pq_ind, newline_ind)
            #debug
            for rang, sent in range_sent.items():
                if rang[0] > rang[1]:
                    print 'rang[0] is: ', rang[0]
                    print 'rang[1] is: ', rang[1]
                    print 'range_sent.items() is: ', range_sent.items()
                    print 'range_sent error \n'
                elif sent[0] is None:
                    print 'range_sent error - sent \n'

            return (range_sent, content, src_ranges)

#
# def get_sentiments(best_path):
#     '''
#     get all sentiments in best files
#     '''
#     if isfile(best_path):
#         best_tree = ET.parse(best_path)
#         best_root = best_tree.getroot()
#
#         sentiment_annotations = best_root.find('sentiment_annotations')
#         elem_names = ['entity', 'relation', 'event']
#         sentiments = []
#         tagids_sarcasm_sentiment = []
#         if sentiment_annotations is not None:
#             for elem_name in elem_names:
#                 for elem_sb in sentiment_annotations.iter(elem_name):
#                     tag_id = elem_sb.get('ere_id')
#                     sentiments_tree = elem_sb.find('sentiments')
#                     for child_et in sentiments_tree.iter('sentiment'):
#                         src_id = None
#                         src_offset = None
#                         src_text = None
#                         polarity = child_et.get('polarity')
#                         sarcasm = child_et.get('sarcasm')
#                         if polarity == 'pos' or polarity == 'neg' or sarcasm == 'yes':
#                             tagids_sarcasm_sentiment.append(tag_id)
#
#                         source = child_et.find('source')
#                         if source is not None:
#                             src_id = source.get('ere_id')
#                             src_offset = int(source.get('offset'))
#                             src_text = source.text
#                         sentiment = (tag_id, src_id, src_offset, polarity, src_text, sarcasm)
#                         sentiments.append(sentiment)
#         return sentiments, tagids_sarcasm_sentiment
#
#
# def get_beliefs(best_path):
#     '''
#     get all beliefs in best files
#     '''
#     if isfile(best_path):
#         best_tree = ET.parse(best_path)
#         best_root = best_tree.getroot()
#         belief_annotations = best_root.find('belief_annotations')
#         elem_names = ['relation', 'event']
#         beliefs = []
#         if belief_annotations is not None:
#             for elem_name in elem_names:
#                 for elem_sb in belief_annotations.iter(elem_name):
#                     tag_id = elem_sb.get('ere_id')
#                     belief_elems = elem_sb.find('beliefs')
#                     # print 'belief_elems is: ', belief_elems
#                     for belief_elem in belief_elems.iter('belief'):
#                         belief_type = belief_elem.get('type')
#                         source = belief_elem.find('source')
#                         sarcasm = belief_elem.get('sarcasm')
#                         src_id = None
#                         src_offset = None
#                         src_text = None
#                         if source is not None:
#                             src_id = source.get('ere_id')
#                             src_offset = int(source.get('offset'))
#                             src_text = source.text
#                         belief = (tag_id, src_id, src_offset, belief_type, src_text, sarcasm)
#                         beliefs.append(belief)
#         return beliefs


def get_smaller_sentences_ranges(sentence):
    '''
    get small chunks of sentences besed on the grammar parsing tree
    '''
    m_t = []
    sp_ranges = []
    small_s_ranges = []
    try:
        t = parser.raw_parse(sentence)
        t = list(t)[0]
        trees = []
        for s in t.subtrees(lambda t: t.label() == 'S'):
            trees.append(s)

        # if len(trees) > 0:
        #     m_t = trees[0].leaves()
        # else:
        m_t = t.leaves()

        if len(trees) > 1:
            small_trees = []
            for i in xrange(len(trees)):
                if i == 0:
                    continue
                r = i + 1
                ex_flag = True
                cur_tw = set(trees[i].leaves())
                while r < len(trees):
                    r_tw = set(trees[r].leaves())
                    r += 1
                    if cur_tw > r_tw:
                        ex_flag = False

                if ex_flag:
                    small_trees.append(trees[i].leaves())

            cur_mt = 0
            is_match = [False]
            for sm_t in small_trees:
                sm_t_r = cur_mt
                # print sm_t
                matches = []
                continuous = 0
                start_ind = 0
                for w in sm_t:
                    while cur_mt < len(m_t) and w != m_t[cur_mt]:
                        is_match.append(False)
                        if is_match[len(is_match) - 2] and continuous != len(sm_t):
                            for m in matches:
                                while cur_mt < len(m_t) and m != m_t[cur_mt]:
                                    cur_mt += 1
                                    is_match.append(False)
                                cur_mt += 1
                                if not is_match[len(is_match)-1]:
                                    start_ind = cur_mt
                                is_match.append(True)
                        else:
                            cur_mt += 1
                            continuous = 0

                    continuous += 1
                    matches.append(w)
                    cur_mt += 1
                    if not is_match[len(is_match)-1]:
                        start_ind = cur_mt
                    if continuous == len(sm_t):
                        small_s_ranges.append([start_ind, cur_mt])
                    is_match.append(True)

            for i, sm_range in enumerate(small_s_ranges):
                if i == 0:
                    mid_range = [1, sm_range[0]-1]
                else:
                    last_range = small_s_ranges[i-1]
                    mid_range = [last_range[1]+1, sm_range[0]-1]

                if not mid_range[0] > mid_range[1]:
                    sp_ranges.append(mid_range)
                sp_ranges.append(sm_range)

                if i == len(small_s_ranges) - 1:
                    mid_range = [sm_range[1]+1, len(m_t)]
                    if not mid_range[0] > mid_range[1]:
                        sp_ranges.append(mid_range)
        else:
            s_range = [1, len(m_t)]
            sp_ranges.append(s_range)

    except Exception:
        pass

    return sp_ranges, m_t, small_s_ranges


def emid_parent_mapper(ere_root):
    '''
    get the entity: entity mentions and entity id: entity_mention ids dicts
    '''
    parent_map = {c:p for p in ere_root.iter() for c in p}
    cid_parent_map = {c.get('id'):p for c, p in parent_map.items() if c.tag == 'entity_mention'}

    return (parent_map, cid_parent_map)


def auh_offsets_pq_ranges(src_ranges):
    '''
    get author offsets and post/quote ranges in the source file
    '''
    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)
    author_inds = src_ranges.author_inds
    post_ranges = src_ranges.post_ranges
    quote_ranges = src_ranges.quote_ranges
    pq_ranges = post_ranges.copy()
    pq_ranges.update(quote_ranges)

    return (author_offsets, pq_ranges)


def author_entities_getter(ere_root, author_offsets, parent_map):
    '''
    get author entities in ere file
    '''
    author_entities = []
    for entm in ere_root.iter('entity_mention'):
        entm_offset = int(entm.get('offset'))
        if entm_offset in author_offsets:
            ent = parent_map[entm]
            author_entities.append(ent)
    author_entities = list(set(author_entities))
    author_entity_ids = [ent.get('id') for ent in author_entities]

    return author_entities, author_entity_ids


def targets_in_sentence(range_sent, emid_offset, cid_parent_map):
    '''
    get all entity_mention ids in each sentence
    '''
    tags_in_sent = {rang : {} for rang in range_sent.keys()}
    for emid, offset in emid_offset.items():
        for sent_range in range_sent.keys():
            if sent_range[0] <= offset < sent_range[1]:
                ent_id = cid_parent_map[emid].get('id')
                if ent_id not in tags_in_sent[sent_range].keys():
                    tags_in_sent[sent_range][ent_id] = [(emid, emid_offset[emid])]
                else:
                    tags_in_sent[sent_range][ent_id].append((emid, emid_offset[emid]))
                break

    return tags_in_sent


def target_info_getter(tag_m, tag_id, target_mention, parent, author_entities, author_offsets,\
        emid_offset, emid_endoffset, fid_offset, fid_endoffset):
    tag_text = ''
    specificity = 2
    tag_range = []
    reg_offsets = []
    p_id = parent.get('id')
    mtype = parent.get('type')
    skip_flag = False
    is_target_author = 0

    if tag_m == 'entity_mention':
        tag_offset = int(target_mention.get('offset'))
        mention_text = target_mention.find('mention_text')
        specificity_str = parent.get('specificity')
        specificity = 1 if specificity_str == 'specific' else 0
        try:
            tag_text = mention_text.text.strip()
        except:
            print 'Warning: the entity_mention with tag_offset {0} does not have mention_text'.format(tag_offset)
        #skip author's entity mentions
        if parent in author_entities:
            is_target_author = 1
            my_words = ['I', 'i', 'my', 'My']
            if tag_text in my_words:
                skip_flag = True

        if tag_offset in author_offsets:
            skip_flag = True

        # p_id = parent.get('id')
        p_type = 'entity'
        # mtype = parent.get('type')
        tag_len = int(target_mention.get('length'))
        subtype = target_mention.get('noun_type')
        tag_range = [tag_offset, tag_offset+tag_len]
        reg_offsets.extend(tag_range)
    elif tag_m == 'relation_mention':
        # p_id = parent.get('id')
        p_type = 'relation'
        # mtype = parent.get('type')
        subtype = parent.get('subtype')
        trigger = target_mention.find('trigger')
        if trigger is not None:
            trigger_offest = int(trigger.get('offset'))
            trigger_endoffest = trigger_offest+int(trigger.get('length'))
            trigger_text = trigger.text
            reg_offsets = [trigger_offest, trigger_endoffest]

        reg1 = target_mention.find('rel_arg1')
        reg2 = target_mention.find('rel_arg2')
        if reg1 is not None:
            reg1_emid = reg1.get('entity_mention_id')
            reg1_offset = emid_offset[reg1_emid]
            reg1_endoffset = emid_endoffset[reg1_emid]
            reg_offsets.append(reg1_offset)
            reg_offsets.append(reg1_endoffset)
        if reg2 is not None:
            reg2_emid = reg2.get('entity_mention_id')
            if reg2_emid is not None:
                reg2_offset = emid_offset[reg2_emid]
                reg2_endoffset = emid_endoffset[reg2_emid]
                reg_offsets.append(reg2_offset)
                reg_offsets.append(reg2_endoffset)
        reg_offsets = sorted(reg_offsets)
        tag_range = [reg_offsets[0], reg_offsets[len(reg_offsets)-1]]
        if trigger is not None:
            tag_offset = trigger_offest
        else:
            tag_offset = tag_range[0]

    elif tag_m == 'event_mention':
        p_type = 'event'
        trigger = target_mention.find('trigger')
        if trigger is not None:
            trigger_offest = int(trigger.get('offset'))
            trigger_endoffest = trigger_offest+int(trigger.get('length'))
            trigger_text = trigger.text
            reg_offsets = [trigger_offest, trigger_endoffest]
        mtype = target_mention.get('type')
        subtype = target_mention.get('subtype')
        regs = target_mention.findall('em_arg')
        for reg in regs:
            arg_mid = reg.get('entity_mention_id')
            if arg_mid is not None:
                arg_offset = emid_offset[arg_mid]
                arg_endoffset = emid_endoffset[arg_mid]
                reg_offsets.append(arg_offset)
                reg_offsets.append(arg_endoffset)
            else:
                arg_mid = reg.get('filler_id')
                if arg_mid is not None:
                    arg_offset = fid_offset[arg_mid]
                    arg_endoffset = fid_endoffset[arg_mid]
                    reg_offsets.append(arg_offset)
                    reg_offsets.append(arg_endoffset)

        reg_offsets = sorted(reg_offsets)
        tag_range = [reg_offsets[0], reg_offsets[len(reg_offsets)-1]]
        if trigger is not None:
            tag_offset = trigger_offest
        else:
            tag_offset = tag_range[0]

    #debug
    if tag_range[0] >= tag_range[1]:
        print 'mention range error ----',tag_range[0],tag_range[1]
    elif tag_range[1] > tag_range[0] + 120:
        print 'memtion range too big ----', tag_range[0],tag_range[1]
        print 'fixing range too big issue-------------\n'
        l = len(reg_offsets) - 1
        while tag_range[1] > tag_range[0] + 100 and l > 0:
            l -= 1
            tag_range[1] = reg_offsets[l]
        tag_offset = tag_range[0]
        reg_offsets = reg_offsets[:l+1]

    return (tag_offset, tag_text, tag_range, reg_offsets, is_target_author, p_type, p_id, mtype, subtype, skip_flag, specificity)

def small_sentence_getter(tokenized_words, target_ranges, src_ind):
    '''
    get small_sentence_w_tag
    '''
    small_sentence_w_tag = ''
    for i, tk_w in enumerate(tokenized_words):
        for t_range in target_ranges:
            if t_range[0] <= i+1 <= t_range[1] and i+1 >= src_ind:
                if tk_w == '''n't''':
                    tk_w = 'not'
                small_sentence_w_tag += ' ' + tk_w
    return small_sentence_w_tag

def words_in_tranges_getter(tokenized_words, target_ranges, target_ind, src_ind):
    words_in_target_ranges = []
    bf_tag_words = []
    af_tag_words = []
    for t_range in target_ranges:
        ind_r = t_range[0]-1
        ind_l = t_range[1]
        words_in_target_range = []
        for i in range(ind_r, ind_l):
            if i < len(tokenized_words) and i+1 >= src_ind:
                w = tokenized_words[i]
                if w == '''n't''':
                    w = 'not'
                words_in_target_range.append(w)
                if i < target_ind-1:
                    bf_tag_words.append(w)
                elif i > target_ind:
                    af_tag_words.append(w)
        words_in_target_ranges.append(words_in_target_range)
    return (words_in_target_ranges, bf_tag_words, af_tag_words)


def get_polar(words_in_target_ranges, all_set, pos_set, neg_set):
    wordnet_lemmatizer = WordNetLemmatizer()
    sentence_list = words_in_target_ranges
    lsts = []
    pos_words = []
    neg_words = []
    for lst in sentence_list:
        lst_current = []
        for word in lst:
            word = word.encode('utf-8')
            try:
                word = str(wordnet_lemmatizer.lemmatize(word))
            except:
                continue
            if word in all_set:
                lst_current.append(word)
            if word in pos_set:
                pos_words.append(word)
            if word in neg_set:
                neg_words.append(word)

        lsts.append(lst_current)
    return (lsts, pos_words, neg_words)

def words_filter(target_text, words_in_tranges, polar_words, parent_type):
    replacement = ''
    words_in_tranges_v2 = []
    words_in_tranges_v3 = []
    pattern = re.compile(re.escape(target_text))

    for i in xrange(len(words_in_tranges)):
        words = words_in_tranges[i]
        polar_num = len(polar_words[i])
        filtered_words_v1 = [w for w in words if w.lower() not in list(string.punctuation)]
        filtered_words_v2 = [w for w in words if w.lower() not in stopwords.words('english')\
                              and w not in list(string.punctuation)]
        if len(filtered_words_v1) > 1:
            words_in_tranges_v2.append(filtered_words_v1)
        if len(filtered_words_v2) > 1 and polar_num != 0:
            words_in_tranges_v3.append(filtered_words_v2)

    small_sentence_v2 = ''
    small_sentence_v3 = ''
    for ws in words_in_tranges_v2:
        for w in ws:
            if small_sentence_v2 == '':
                small_sentence_v2 = w
            else:
                small_sentence_v2 += ' ' + w
    for ws in words_in_tranges_v3:
        for w in ws:
            if small_sentence_v3 == '':
                small_sentence_v3 = w
            else:
                small_sentence_v3 += ' ' + w

    if parent_type == "entity":
        replacement = "HE"
    elif parent_type == "event":
        replacement = "THIS"
    else:
        replacement = "THING"
    small_sentence_v2 = small_sentence_v2.replace(target_text, replacement)
    small_sentence_v3 = small_sentence_v3.replace(target_text, replacement)

    return (small_sentence_v2, small_sentence_v3,words_in_tranges_v2, words_in_tranges_v3)


def mention_bw(p_emwinds, target_index, src_ind, target_ranges, polar_words, cid_parent_map):
    num_polars = 0
    for pw in polar_words:
        num_polars += len(pw)

    num_bw = 0
    num_in = 0
    for ent, indoffset_list in p_emwinds.items():
        if 'r-' not in ent:
            for indoffset in indoffset_list:
                ind = indoffset[0]
                offset = indoffset[1]
                emid = indoffset[2]
                try:
                    em_p = cid_parent_map[emid]
                    em_mtype = em_p.get('type')
                except:
                    em_mtype = 'LOC'

                for rg in target_ranges:
                    if rg[0] <= ind <=rg[1] and src_ind < ind < target_index:
                        num_bw += 1
                        if rg[0] <= target_index <=rg[1]:
                            num_in += 1
                        break
    return (num_bw, num_in, num_polars)


def create_dataset(source_path, ere_path, fname, sentiment_set):
    '''
    create a train/test dataset in json format from original source and xml files
    '''
    all_set = sentiment_set['all_set']
    pos_set = sentiment_set['pos_set']
    neg_set = sentiment_set['neg_set']

    range_sent, content, src_ranges = get_sent_ranges(source_path)
    author_offsets, pq_ranges = auh_offsets_pq_ranges(src_ranges)

    if isfile(ere_path):
        ere_tree = ET.parse(ere_path)
        ere_root = ere_tree.getroot()
        parent_map, cid_parent_map = emid_parent_mapper(ere_root)
        offset_emid, emid_offset, emid_endoffset, relm_offset, fid_offset, fid_endoffset = mid_offset_dicts_getter(ere_root) #ere_tree
        author_entities, author_entity_ids = author_entities_getter(ere_root, author_offsets, parent_map)

        #get all entity_mention ids in each sentence
        tags_in_sent = targets_in_sentence(range_sent, emid_offset, cid_parent_map)

        tag_sentences = []
        tag_ms = ['entity_mention', 'relation_mention', 'event_mention']

        for tag_m in tag_ms:
            for target_mention in ere_root.iter(tag_m):
                #target info
                tag_id = target_mention.get('id')
                tag_text = ''
                tag_range = []
                target_ranges = []
                not_target_inds = []
                tag_offset = None
                target_ind = None
                tag_type = None
                target_inds_v2 = []
                not_target_ranges_v2 = []
                not_target_inds_v2 = []
                parent = parent_map[target_mention]

                #source info
                src_text = None
                src_offset = None
                src_id = None
                src_is_author = 1
                src_ind = 0
                src_pid = 'none'
                src_mtype = 'none'

                #sentence info
                sentence = ''
                sentence_w_tags = ''
                wps = {} #word positions

                #entity mention's parent entity info
                p_id = None
                p_type = ''
                p_emoffsets = {}
                p_emwinds = {}
                #change p_emwinds into {u'h-1043': [(6, 1490, em-103)], u'ent-270': [13]}
                parent = parent_map[target_mention]

                #others
                pq_ind = None
                newline_ind = None
                src_tag_dis = None


                tag_offset, tag_text, tag_range, reg_offsets, is_target_author, p_type, p_id,\
                mtype, subtype, skip_flag, specificity = target_info_getter(tag_m, tag_id, target_mention, parent, author_entities,\
                author_offsets, emid_offset,emid_endoffset, fid_offset, fid_endoffset)

                if skip_flag:
                    continue

                for s_range, sent in range_sent.items():
                    s_range_l = int(s_range[0])
                    s_range_r = int(s_range[1])
                    if s_range_l <= tag_offset < s_range_r:
                        for srcoffset, ranges in pq_ranges.items():
                            for rg in ranges:
                                if rg[0] <= s_range_l and s_range_r <= rg[1]:
                                    src_offset = int(srcoffset)
                                    if src_offset in offset_emid.keys():
                                        src_id = offset_emid[src_offset]
                                        src_endoffset = emid_endoffset[src_id]
                                        src_text = content[src_offset:src_endoffset]
                        tag_range1 = s_range_l
                        tag_range2 = s_range_r
                        for reg_offset in reg_offsets:
                            if s_range_l <= reg_offset:
                                tag_range1 = reg_offset
                                break
                        for reg_offset in sorted(reg_offsets, reverse=True):
                            if s_range_r >= reg_offset:
                                tag_range2 = reg_offset
                                break
                        tag_range = [tag_range1, tag_range2]
                        if tag_text == '':
                            tag_text = content[tag_range1:tag_range2]
                        # sent_content = sent[0]
                        sentence = remove_tags(content[s_range_l:s_range_r])
                        pq_ind = sent[1]
                        newline_ind = sent[2]
                        emid_offsets = []
                        #to get the dict of ent_id : corresponding em indexs for coreference
                        for ent_id, emids in tags_in_sent[s_range].items():
                            for emid in emids:
                                e_offset = emid[1]
                                emid_offsets.append(e_offset)
                                if ent_id not in p_emoffsets.keys():
                                    p_emoffsets[ent_id] = [e_offset]
                                else:
                                    p_emoffsets[ent_id].append(e_offset)

                        if p_id not in p_emoffsets.keys():
                            p_emoffsets[p_id] = [tag_offset]
                            emid_offsets.append(tag_offset)

                        emid_offsets.append(s_range_r)
                        emid_offsets.append(tag_offset)
                        emid_offsets = list(set(emid_offsets))
                        emid_offsets = sorted(emid_offsets)
                        # print 'emid_offsets is: ', emid_offsets
                        emid_offsets_len = len(emid_offsets)
                        for i in xrange(emid_offsets_len):
                            if i == 0:
                                # print 'now emid_offsets[i] is: ', emid_offsets[i]
                                sentence_w_tags = remove_tags(content[s_range_l:emid_offsets[i]])
                            elif i < emid_offsets_len:
                                # print 'now emid_offsets[i] is: ', emid_offsets[i]
                                sentence_w_tags += ' MENTION_INDICATOR' + remove_tags(content[emid_offsets[i-1]:emid_offsets[i]]).strip()


                        words = stanford_tokenizer.tokenize(sentence_w_tags)
                        # print 'words is: ', words
                        mw_inds = [i+1 for i, w in enumerate(words) if 'MENTION_INDICATOR' in w]
                        mw_counts = [w.count('MENTION_INDICATOR') for i, w in enumerate(words) if 'MENTION_INDICATOR' in w]
                        for i in xrange(len(mw_inds)):
                            count = mw_counts[i]
                            while count > 1:
                                mw_inds.append(mw_inds[i])
                                count -= 1
                        mw_inds = sorted(mw_inds)
                        flated_inds = [i for inds in p_emoffsets.values() for i in inds]
                        flated_inds = sorted(list(set(flated_inds)))
                        ind_ranks = {ind: i for i, ind in enumerate(flated_inds)}
                        # print 'flated_inds is: ', flated_inds
                        # print 'mw_inds is: ', mw_inds
                        # print 'tag_text is: ', tag_text
                        if len(flated_inds) == len(mw_inds):
                            offset_inds = zip(flated_inds, mw_inds)
                            target_ind = [offset_ind[1] for offset_ind in offset_inds if offset_ind[0] == tag_offset]
                            try:
                                target_ind = target_ind[0]
                                # print 'target_ind is: ',target_ind
                            except:
                                print 'sentence is: ', sentence
                                print 'tag_text is: ',target_ind
                                print 'target_ind can not be found!------------\n'
                            not_target_inds = [ind for ind in mw_inds if ind != target_ind]
                            if not_target_inds == None:
                                not_target_inds = []
                            # print 'not_target_inds after removing target_ind: ', not_target_inds
                            for p, emoffsets in p_emoffsets.items():
                                ent_tups = []
                                offset_ind_tups = []
                                emwinds = []
                                for emoffset in emoffsets:
                                    offset_ind_tup = [offset_ind for offset_ind in offset_inds if offset_ind[0] == emoffset]
                                    offset_ind_tups.append(offset_ind_tup[0])

                                for tup in offset_ind_tups:
                                    if tup[0] in offset_emid.keys():
                                        emwinds.append((tup[0], tup[1], offset_emid[tup[0]]))
                                    else:
                                        emwinds.append((tup[0], tup[1], 'em_or_relm'))
                                p_emwinds[p] = emwinds
                                # p_emwinds[p] = [(tup[0], tup[1], offset_emid[tup[0]]) for tup in offset_ind_tups]
                        else:
                            print 'Annotation index error, the sentence is ignored.'
                            print 'tag_offset is: ', tag_offset
                            print 'words is: ', words
                            print 'flated_inds is: ', flated_inds
                            print 'mw_inds is: ', mw_inds
                            print 'tag_text is: ', tag_text

                        all_emids = tags_in_sent[s_range]
                        break

                #debug
                # print p_emoffsets
                if sentence == '':
                    print 'range_sent keys are: ', range_sent.keys()
                    print "the tag_offset {0} is not in any sentences and sentence is empty------ \n".format(tag_offset)
                    continue
                if tag_text == '' and sentence != '':
                    print 'the tag text is empty: ', tag_offset, tag_id

                if sentence != '':
                    for p, ew in p_emwinds.items():
                        if p != p_id:
                            not_target_inds_v2.extend(ew)
                        else:
                            target_inds_v2 = ew

                    tokenized_words = stanford_tokenizer.tokenize(sentence)
                    # print 'tokenized_words is: ',tokenized_words
                    for i, word in enumerate(tokenized_words):
                        if word not in wps.keys():
                            wps[word] = [(word, i+1)]
                        else:
                            wps[word].append((word, i+1))

                    if len(tokenized_words) > 150:
                        print 'tokenized_words is bigger than 150-------------------\n'
                        t_start = target_ind-8 if target_ind-8 >= 0 else 0
                        t_end = target_ind+8 if target_ind+8 < len(tokenized_words) else len(tokenized_words)-1
                        new_sw = tokenized_words[t_start:t_end]
                        new_s = ''
                        target_ind = 9
                        for w in new_sw:
                            new_s += ' ' + w
                        sentence = new_s
                        tokenized_words = stanford_tokenizer.tokenize(sentence)
                        for i, word in enumerate(tokenized_words):
                            if word not in wps.keys():
                                wps[word] = [(word, i+1)]
                            else:
                                wps[word].append((word, i+1))

                    splited_s_ranges, m_t, small_s_ranges = get_smaller_sentences_ranges(sentence)

                    if len(splited_s_ranges) == 0 and len(m_t) == 0:
                        continue

                    if len(tokenized_words) != len(m_t):
                        print 'stanford parser len {0} is not the same as stanford_tokenizer.tokenize len {1} ----'.format(len(m_t), len(tokenized_words))
                        print 'stanford_tokenizer.tokenize is: ', tokenized_words
                        print 'stanford tree is: {0}---\n'.format(m_t)

                    target_range = []
                    for sp_range in splited_s_ranges:
                        sp_range_l = sp_range[0]
                        sp_range_r = sp_range[1]
                        if sp_range_l <= target_ind <= sp_range_r:
                            target_range = sp_range


                    if len(target_range) != 0:
                        target_range_ind = splited_s_ranges.index(target_range)
                    else:
                        rg_len = len(splited_s_ranges)
                        target_range_ind = rg_len-2
                        try:
                            l = rg_len
                            tag_firstw = tag_text.split()[0]
                            while l > 0:
                                l -= 1
                                target_range = splited_s_ranges[l]
                                tag_range_words = tokenized_words[target_range[0]-1:target_range[1]]
                                i = 0
                                for w in tag_range_words:
                                    i += 1
                                    if w == tag_firstw:
                                        target_ind = target_range[0] + i
                                        target_range_ind = l
                                        break
                        except:
                            print 'sentence can not find any target_range: ', sentence
                            print 'target_ind is: ', target_ind
                            print 'target_text is: ', tag_text
                            print 'target_range is: ', target_range
                            print 'splited_s_ranges is {0} ------\n'.format(splited_s_ranges)
                            # continue

                    smaller_sent_range_inds = [target_range_ind-2,target_range_ind-1,target_range_ind, target_range_ind+1, target_range_ind+2]
                    for i in smaller_sent_range_inds:
                        if 0 <= i < len(splited_s_ranges):
                            pos_trange = splited_s_ranges[i]
                            pos_range_l = pos_trange[0]
                            pos_range_r = pos_trange[1]
                            add_flag = True
                            for nt_ind in not_target_inds:
                                if pos_range_l <= nt_ind <= pos_range_r \
                                and (pos_trange in small_s_ranges or pos_range_r-pos_range_l > 10)\
                                and not (pos_range_l <= target_ind <= pos_range_r):
                                    add_flag = False
                                    break
                            if add_flag:
                                target_ranges.append(pos_trange)
                    # print 'target_ranges is ', target_ranges
                    sources_tobe_added = {}
                    sources_tobe_added[src_id] = [src_id, str(src_offset), src_text, str(src_ind), str(src_is_author)]


                    for src_tobe_added in sources_tobe_added.values():
                        src_id = src_tobe_added[0]
                        src_offset = src_tobe_added[1]
                        src_text = src_tobe_added[2]
                        src_ind = int(src_tobe_added[3])
                        src_is_author = int(src_tobe_added[4])

                        try:
                            src_p = cid_parent_map[src_id]
                            src_pid = src_p.get('id')
                            src_mtype = src_p.get('type')
                        except:
                            src_pid = 'none'
                            src_mtype = 'none'

                        small_sentence_w_tag = small_sentence_getter(tokenized_words, target_ranges, src_ind)
                        words_in_target_ranges, bf_tag_words, af_tag_words = words_in_tranges_getter(tokenized_words, target_ranges, target_ind, src_ind)

                        #new total_words
                        target_tokens = stanford_tokenizer.tokenize(tag_text)

                        limit = 5
                        before_target_words_v2 = []
                        after_target_words_v2 = []
                        for i, w in enumerate(reversed(bf_tag_words)):
                            if w not in target_tokens:
                                before_target_words_v2.insert(0, w)
                                if len(before_target_words_v2) == 5:
                                    break
                        for i, w in enumerate(af_tag_words):
                            if w not in target_tokens:
                                after_target_words_v2.append(w)
                                if len(after_target_words_v2) == 5:
                                    break

                        total_words = []
                        total_words.extend(before_target_words_v2)
                        total_words.extend(target_tokens)
                        total_words.extend(after_target_words_v2)

                        polar_words, pos_words, neg_words = get_polar(words_in_target_ranges ,all_set, pos_set, neg_set)
                        small_sentence_v2, small_sentence_v3, words_in_tranges_v2, words_in_tranges_v3 = words_filter(tag_text, words_in_target_ranges, polar_words, p_type)
                        num_bw, num_in, num_polars = mention_bw(p_emwinds, target_ind, src_ind, target_ranges, polar_words, cid_parent_map)
                        if target_ind != None and src_ind != None:
                            src_tag_dis = target_ind - src_ind

                        tag_sentence = {'file_name': fname, 'original_sentence': sentence, 'tokenized_sentence': tokenized_words,\
                                        'sentence_targets_indicator': sentence_w_tags, 'small_sentence_w_target': small_sentence_w_tag,\
                                        'target_id': tag_id, 'target_text': tag_text, 'target_index': target_ind, 'target_offset': tag_offset,\
                                        'source_offset': src_offset, 'source_is_author': src_is_author, 'source_pid': src_pid,\
                                        'source_mtype': src_mtype, 'source_text': src_text, 'source_id': src_id, 'source_index': src_ind,\
                                        'parent_id': p_id, 'parent_type': p_type, 'mtype': mtype, 'subtype': subtype,\
                                        'pq_ind': pq_ind, 'newline_ind': newline_ind,\
                                        'entid_emoffsets': p_emoffsets, 'words_in_target_ranges': words_in_target_ranges,\
                                        'splited_s_ranges': splited_s_ranges, 'target_ranges': target_ranges,\
                                        'wps': wps, 'target_inds_v2': target_inds_v2, 'not_target_inds': not_target_inds,\
                                        'entid_em_indoffsets': p_emwinds, 'target_is_author_mention': is_target_author,'source_target_dis': src_tag_dis,\
                                        'before_target_words': bf_tag_words, 'after_target_words': af_tag_words,\
                                        'pos_words_in_target_ranges': pos_words, 'neg_words_in_target_ranges': neg_words,\
                                        'sentiment_words_in_target_ranges': polar_words, 'small_sentence_v2': small_sentence_v2,\
                                        'small_sentence_v3': small_sentence_v3, 'words_in_tranges_v2': words_in_tranges_v2,\
                                        'words_in_tranges_v3': words_in_tranges_v3, 'num_mention_bw_st': num_bw, 'num_mention_in_st': num_in,\
                                        'num_polar_words': num_polars, 'before_target_words_v2': before_target_words_v2, 'after_target_words_v2':after_target_words_v2,\
                                        'five_words_bw_target': total_words, 'specificity': specificity}

                        tag_sentences.append(tag_sentence)

    return tag_sentences


def transfrom_all_indir(input_dir):
    """
    process all source, ere, and annotation files in the input dir, and write all new source,
    ere, and annotation files to source_new, ere_new, and annotation_new folders in this dir.
    """
    if not isdir(input_dir):
        usage()
    else:
        src_input_dir = os.path.join(input_dir,"source")
        ere_input_dir = os.path.join(input_dir,"ere")
        output_dir = os.path.join(input_dir,"output")
        all_set = pickle.load( open( all_dir, "rb" ) )
        neg_set = pickle.load( open( neg_dir, "rb" ) )
        pos_set = pickle.load( open( pos_dir, "rb" ) )
        sentiment_set = dict(all_set=all_set, neg_set=neg_set, pos_set=pos_set)
        json_output = os.path.join(input_dir,"real_test_nw.json")

        #if dirs do not exist, makedirs
        if not exists(output_dir):
            makedirs(output_dir)

        total_tag_sentences = []
        #find all file names
        # filenames = [f for f in os.listdir(src_input_dir) if f.endswith('.cmp.txt')]
        # filenames_xml = [f for f in os.listdir(ere_input_dir) if f.startswith('APW_ENG')\
        #                     or f.startswith('ENG_DF') or f.startswith('NYT_ENG') or f.startswith('AFP_ENG')]
        # filenames.extend(filenames_xml)
        filenames = [f for f in os.listdir(ere_input_dir) if f.startswith('ENG_NW')\
                     or f.startswith('NYT_ENG') or f.startswith('AFP_ENG') or f.startswith('ENG_DF')]

        for f in filenames:
            base_name = ''
            src_fn = ''
            ere_fn = ''
            if f.endswith('.cmp.txt'):
                base_name = f.replace('.cmp.txt','')
                src_fn = f
                ere_fn = '{0}.rich_ere.xml'.format(base_name)
            elif f.endswith('.predicted.map.rich_ere.xml'):
                base_name = f.replace('.predicted.map.rich_ere.xml','')
                if f.startswith('ENG_DF'):
                    f_name = f[:32]
                else:
                    f_name = base_name
                src_fn = '{0}.xml'.format(f_name)
                ere_fn = f

            print "processing file is: {0} \n".format(base_name)
            src_path = os.path.join(src_input_dir, src_fn)
            ere_path = os.path.join(ere_input_dir, ere_fn)

            #if ere and ere files with the same name as source file exist
            if isfile(src_path) and isfile(ere_path):
                tag_sentences = create_dataset(src_path, ere_path, base_name, sentiment_set)
                # write new json file
                output_fname = base_name + '.json'
                output_file = os.path.join(output_dir,output_fname)
                with codecs.open(output_file, 'w', 'utf8') as of:
                    of.write(json.dumps(tag_sentences, sort_keys = True, ensure_ascii=False))

                total_tag_sentences.extend(tag_sentences)

                print 'in file {0} ---------------'.format(base_name)
                print 'there are {0} sentences.\n'.format(len(tag_sentences))

        print 'there are total {0} sentences.\n'.format(len(total_tag_sentences))

        # write new json file
        with codecs.open(json_output, 'w', 'utf8') as f:
            f.write(json.dumps(total_tag_sentences, sort_keys = True, ensure_ascii=False))

        print('Successfully write the new json file to {0}.'.format(input_dir))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        transfrom_all_indir(sys.argv[1])
