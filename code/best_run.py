import os
import argparse
import subprocess
import numpy as np
import pandas as pd
import pickle
import scipy.sparse as sp
from scipy.sparse import csr_matrix
from collections import Counter
from sklearn.metrics.scorer import make_scorer
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, f1_score, classification_report
from nltk import pos_tag, word_tokenize
from sklearn.datasets import dump_svmlight_file

from data_preprocessing import add_lex_indicator
from feature_extractor import feature_extract
from utils import load_bin_vec, read_dataset, tokenize, compute_pos_wemb, glove2dict
from to_pred_best import build_bestxml
from best_evaluator0819 import process_predict_roots, PrivateStateTuple, score_pst_tuples

# gold_dir =
# ere_dir =
# pred_dir = 

def usage():
    print '#####NBSVM + POS embedding model#####'
    print 'python nbsvm_pos.py --train [path to train in json] --test [path to test in json] --we [path to word2vec] --ngram [e.g. 123]'


def fit_classifier_with_crossvalidation(X, y, basemod, cv, param_grid, scoring='f1_micro'):
    '''
    Fit classifier using cross validation

    ---Parameters---

    X, y: training feature matrix, labels
    basemodel: base model e.g. LogisticRegression()
    param_grid: tunning parameters of basemodel
    cv: cross validation folders

    ---Returns---

    best_model: best model return by cross validation

    '''

    # Find the best model within param_grid:
    crossvalidator = GridSearchCV(basemod, param_grid, cv=cv, scoring=scoring)
    crossvalidator.fit(X, y)

    # Report some information:
    print '\n-----------Grid scores-------------'
    for params, mean_score, scores in crossvalidator.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() * 2, params))

    print("Best params", crossvalidator.best_params_)
    print("Best score: %0.03f" % crossvalidator.best_score_)
    # Return the best model found:
    best_model = crossvalidator.best_estimator_

    return best_model


def build_counters(df, grams, coln_t='text_lex', coln_y='has_sent_target'):
    '''
    Build dictionary of tokens for each class in the training data set

    ---Parameters---

    df: training set with attributes y: label, text_lex: text after data preprocessing
    grams : a n-grams rule string (like "123")

    ---Returns---

    counters: the token counter for each class
    '''
    grams = [int(i) for i in grams]
    counters = {}
    count = 0
    for _, rev in df.iterrows():
        c = rev[coln_y]
        text = rev[coln_t]
        count += 1
        # Select class counter
        if c not in counters:
            # we don't have a counter for this class
            counters[c] = Counter()
        counter = counters[c]

        # update counter
        counter.update(tokenize(text, grams))

    print 'number of datum in train: ', count

    return counters


def compute_ratios(counters, alpha=1.0):
    '''
    Calculates the log-count ratios of each token for each class in the training data set

    ---Parameters---

    counters: the token counter for each class
    alpha : smoothing parameter in count vectors (default 1)

    ---Returns---

    dic: a dictionary from token to tokens index
    ratios: log-count ratio
    v: total number of tokens
    '''
    ratios = dict()

    # create a vocabulary - a list of all ngrams
    all_ngrams = set()
    for counter in counters.values():
        all_ngrams.update(counter.keys())
    all_ngrams = list(all_ngrams)
    v = len(all_ngrams)  # the ngram vocabulary size

    # a standard NLP dictionay (ngram -> index map) use to update the
    # one-hot vector p
    dic = dict((t, i) for i, t in enumerate(all_ngrams))

    # sum ngram counts for all classes with alpha smoothing
    # 2* because one gets subtracted when q_c is calculate by subtracting p_c
    sum_counts = np.full(v, 2*alpha)
    for c in counters:
        counter = counters[c]
        for t in all_ngrams:
            sum_counts[dic[t]] += counter[t]

    # calculate r_c for each class
    for c in counters:
        counter = counters[c]
        p_c = np.full(v, alpha)     # initialize p_c with alpha (smoothing)

        # add the ngram counts
        for t in all_ngrams:
            p_c[dic[t]] += counter[t]

        # initialize q_c
        q_c = sum_counts - p_c

        # normalize (l1 norm)
        p_c /= np.linalg.norm(p_c, ord=1)  # = p_c / sum(p_c)
        q_c /= np.linalg.norm(q_c, ord=1)

        # p_c = log(p/|p|)
        p_c = np.log(p_c)
        # q_c = log(not_p/|not_p|)
        q_c = np.log(q_c)

        # Subtract log(not_p/|not_p|
        ratios[c] = p_c - q_c

    return dic, ratios, v


def process_files_ngram(df, dic, r, v, grams, coln_t='text_lex'):
    '''
    Process dataframe file to get log-count ngrams ratio vectors
    (feature input for original nbsvm)

    ---Parameters---

    df: the dataframe input file
    dic, r, v: returns from compute_ratio function
    grams: the ngrams rule (like [1,2,3]) (grams)
    coln_t, coln_y: column names for text and label

    ---Returns---

    log_counts: log-count ngrams ratio vectors
    '''

    grams = [int(i) for i in grams]
    n_samples = df.shape[0]
    classes = r.keys()
    X = dict()
    data = dict()
    indptr = [0] # it's a index for the head of doc in indices
    indices = [] # it's a token index list
    for c in classes:
        data[c] = []

    for i, d in df.iterrows():
        text = d[coln_t]
        ngrams = tokenize(text, grams)
        for g in ngrams:
            if g in dic:
                index = dic[g]
                indices.append(index)
                for c in classes:
                    data[c].append(r[c][index])
        indptr.append(len(indices))

    for c in classes:
        X[c] = csr_matrix((data[c], indices, indptr), shape=(n_samples, v), dtype=np.float32)

    if len(classes) == 2:
        log_counts = X[1]
    else:
        log_counts = sp.hstack(X.values(), format='csr')

    return log_counts


def column_mean(values):
    return values.mean()

def add_is_keep_parag_t(df, df_tpp):
    newline_ind = df['newline_ind']
    fname = df['file_name']
    return df_tpp.ix[(newline_ind, fname)]

def add_is_keep_post_t(df, df_tpp):
    fname = df['file_name']
    pq_ind = df['pq_ind']
    return df_tpp.ix[(pq_ind, fname)]

def add_is_keep_file_t(df, df_tf):
    fname = df['file_name']
    return df_tf.ix[fname]

def add_is_keep_auh_t(df, df_tauh):
    source_pid = df['source_pid']
    file_name = df['file_name']
    return df_tauh.ix[(source_pid, file_name)]


def process_files_wemb(df, wemb):
    '''
    Process dataframe file to get POS wembedding features in sparse matrix

    ---Parameters---

    df: dataframe file
    wemb: word embedding look up table (eg: word2vec)
    coln: column name for adding wembedding (eg: text)

    ---Returns---

    wemb_csr: sparse matrix of POS word embedding features

    '''

    coeff=[1/3.0, 1/3.0, 1/3.0]#depreciated
    start_index = 0
    wemb_f = df['small_sentence_w_target'].apply((lambda x: compute_pos_wemb(x, wemb, coeff, start_index)))
    for i in xrange(start_index, start_index+900):
        df[str(i)] = wemb_f[i]

    # start_index = 900
    # wemb_f = df['before_target_words'].apply((lambda x: compute_pos_wemb(x, wemb, coeff, start_index)))
    # for i in xrange(start_index, start_index+900):
    #     df[str(i)] = wemb_f[i]
    #
    # start_index = 1800
    # wemb_f = df['after_target_words'].apply((lambda x: compute_pos_wemb(x, wemb, coeff, start_index)))
    # for i in xrange(start_index, start_index+900):
    #     df[str(i)] = wemb_f[i]

    start_index = 900
    wemb_f = df['original_sentence'].apply((lambda x: compute_pos_wemb(x, wemb, coeff, start_index)))
    for i in xrange(start_index, start_index+900):
        df[str(i)] = wemb_f[i]

    features_mean= [str(i) for i in range(900, 1800)]
    #paragraph word embedding
    # df_tpp = df.groupby(['newline_ind', 'file_name'])[features_mean].agg(column_mean)
    # iskpp_inds = [str(i) for i in range(1800, 2700)]
    # df[iskpp_inds] = df.apply((lambda x: add_is_keep_parag_t(x, df_tpp)), axis=1)
    #post word embedding
    df_tpp = df.groupby(['pq_ind', 'file_name'])[features_mean].agg(column_mean)
    iskpp_inds = [str(i) for i in range(1800, 2700)]
    df[iskpp_inds] = df.apply((lambda x: add_is_keep_post_t(x, df_tpp)), axis=1)
    #file word embedding
    df_tf = df.groupby(['file_name'])[features_mean].agg(column_mean)
    iskpf_inds = [str(i) for i in range(2700, 3600)]
    df[iskpf_inds] = df.apply((lambda x: add_is_keep_file_t(x, df_tf)), axis=1)
    #author word embedding
    # df_tauh = df.groupby(['source_pid', 'file_name'])[features_mean].agg(column_mean)
    # iskpauh_inds = [str(i) for i in range(3600, 4500)]
    # df[iskpauh_inds] = df.apply((lambda x: add_is_keep_auh_t(x, df_tauh)), axis=1)

    #convert all word embedding features into sparse matrix
    wemb_ind = [str(i) for i in range(0, 3600)]
    print "In embed process --- the embed matrix shape:", df[wemb_ind].shape
    wemb_csr = csr_matrix(df[wemb_ind])

    return wemb_csr


def pred_evaluation(
    pred_pd,
    gold_dir,
    ere_dir,
    sentiment_flag,
    belief_flag,
    null_source_flag=False,
    pred_dir=None):
    '''
    Running model

    ---Parameters---

    basemodel: base model e.g. LogisticRegression()
    param_grid: tunning parameters of basemodel
    cv: cross validation folders
    train, test: training and testing feature sparse matrices
    y_train, y_test: gold labels for training/testing data set
    coln: column name for adding wembedding (eg: text)

    ---Returns---

    wemb_csr: sparse matrix of POS word embedding features

    '''

    grouped_fname = pred_pd.groupby(['file_name'])
    pred_roots = build_bestxml(grouped_fname, sentiment_flag, belief_flag, pred_dir)

    scores_by_file, averaged_scores = process_predict_roots(ere_dir, gold_dir, pred_roots,\
                        sentiment_flag=sentiment_flag, belief_flag=belief_flag, null_source_flag=null_source_flag)

    return scores_by_file, averaged_scores


def scoring_func(arr1, arr2):
    '''
    calculating P, R and F score of common items of two lists
    '''

    arr3 = zip(arr1, arr2)
    true_pos = 0
    gold_count = 0
    pred_count = 0
    for pair in arr3:
        if pair[0] == 1:
            gold_count += 1
        if pair[1] == 1:
            pred_count += 1
        if pair[0] == pair[1] and pair[0] == 1:
            true_pos += 1

    precision = true_pos / float(pred_count) if pred_count > 0 else 1.0
    recall = true_pos / float(gold_count) if gold_count > 0 else 1.0
    fscore = 2 * (precision * recall) / (precision + recall) if (precision + recall) > 0 else 0.0

    return fscore


def model_run(model, X_test, test_df, test):
    '''
    Running model

    ---Parameters---

    basemodel: base model e.g. LogisticRegression()
    param_grid: tunning parameters of basemodel
    cv: cross validation folders
    train, test: training and testing feature sparse matrices
    y_train, y_test: gold labels for training/testing data set
    coln: column name for adding wembedding (eg: text)

    ---Returns---

    wemb_csr: sparse matrix of POS word embedding features

    '''

    print('testing...')
    y_pred = model.predict(X_test)

    print '\n-------------pred y_dev counts------------'
    unique, counts = np.unique(y_pred, return_counts=True)
    print np.asarray((unique, counts)).T

    #mapping numerical y_pred into 'neg', 'pos'
    pred_sentiment = ['pos' if i == 2 else 'neg' if i == 1 else 'none' for i in y_pred]
    test_df['pred_sentiment'] = pred_sentiment

    grouped_fname = test_df.groupby(['file_name'])
    sentiment_flag = True
    belief_flag = False
    pred_roots = build_bestxml(grouped_fname, sentiment_flag, belief_flag, test)
    print('Testing Done! Successfully write the predicted best xml files to {0}.'.format(test))


def main(test, ngram, we):
    '''
    Given output path (out), liblinear path (liblinear),
    Given ngram string rule (like "123"), ngram
    '''

    print 'loading train model pkg...'
    dic, r, v, model = pickle.load( open('../data/train_pkg.p', "rb" ) )

    print 'loading testing data and extracting features from source, ere, and files...'
    test_df = feature_extract(test, False)
    test_df = test_df[test_df['source_is_author']==1]

    print 'cleaning data and add lex indicators...'
    test_df[['text_lex', 'lex_ws']] = test_df.apply(add_lex_indicator, axis=1)

    print 'loading word embedding...'
    word2vec = load_bin_vec(we) #glove2dict(we) #

    print "building test features --- ngram part..."
    test_df.sort_index(inplace=True)
    X_test_ngram = process_files_ngram(test_df, dic, r, v, ngram)

    print "building test features --- pos embedding part..."
    X_test_embed = process_files_wemb(test_df, word2vec)

    print "combining log-count ratio and pos embedding features..."
    X_test = sp.hstack((X_test_ngram, X_test_embed), format='csr')

    print "running model..."
    model_run(model, X_test, test_df, test)



if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description='Run NB-SVM on some text files.')
        parser.add_argument('--test', help='path to test dir where contains ere and source folders')
        parser.add_argument('--ngram', help='N-grams considered e.g. 123 is uni+bi+tri-grams')
        parser.add_argument('--we', help='path to word embedding e.g. word2vec')
        args = vars(parser.parse_args())
    except:
        usage()

    main(**args)
